CREATE TABLE solicitudes (
    id SERIAL PRIMARY KEY,
    tipo_solicitud VARCHAR(50) NOT NULL,
    institucion_emisora VARCHAR(100) NOT NULL,
    solicitante_id INT NOT NULL,
    fecha_emision TIMESTAMP NOT NULL,
    estado VARCHAR(20) NOT NULL,
    total_items NUMERIC(10, 2) NOT NULL,
    peso_total NUMERIC(10, 2) NOT NULL,
    detalles_pago JSONB NOT NULL, -- Campo de tipo JSONB para detalles de pago
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO solicitudes (
    tipo_solicitud,
    institucion_emisora,
    solicitante_id,
    fecha_emision,
    estado,
    total_items,
    peso_total,
    detalles_pago
) VALUES (
    'certificado',
    'Ministerio de Transporte',
    12345,
    '2024-05-26 10:00:00',
    'pendiente',
    5,
    3000.50,
    '{
        "nit": "123456789",
        "razon_social": "Empresa Importadora S.A.",
        "monto": 1500.75
    }'
);
INSERT INTO solicitudes (
    tipo_solicitud,
    institucion_emisora,
    solicitante_id,
    fecha_emision,
    estado,
    total_items,
    peso_total,
    detalles_pago
) VALUES
(
    'certificado',
    'Ministerio de Transporte',
    12345,
    '2024-05-26 10:00:00',
    'pendiente',
    5,
    3000.50,
    '{
        "nit": "123456789",
        "razon_social": "Empresa Importadora S.A.",
        "monto": 1500.75
    }'
),
(
    'certificado',
    'Ministerio de Salud',
    67890,
    '2024-05-27 11:30:00',
    'aprobado',
    10,
    4500.00,
    '{
        "nit": "987654321",
        "razon_social": "Distribuidora Médica S.A.",
        "monto": 3200.00
    }'
),
(
    'declaracion jurada',
    'Ministerio de Economía',
    11223,
    '2024-05-28 09:15:00',
    'rechazado',
    3,
    1200.75,
    '{
        "nit": "555666777",
        "razon_social": "Comercializadora XYZ",
        "monto": 500.00
    }'
),
(
    'permiso',
    'Ministerio de Agricultura',
    44556,
    '2024-05-29 14:45:00',
    'pendiente',
    7,
    2500.30,
    '{
        "nit": "444333222",
        "razon_social": "Agroexportadora ABC",
        "monto": 2000.00
    }'
),
(
    'autorizacion previa',
    'Ministerio de Energía',
    77889,
    '2024-05-30 16:00:00',
    'aprobado',
    4,
    1800.90,
    '{
        "nit": "888999000",
        "razon_social": "Energía y Servicios E.S.",
        "monto": 1200.50
    }'
);


SELECT * FROM solicitudes;
SELECT * FROM solicitudes WHERE institucion_emisora = 'Ministerio de Transporte';
SELECT * FROM solicitudes WHERE tipo_solicitud = 'certificado';
SELECT * FROM solicitudes WHERE estado = 'aprobado';
-- JSON
SELECT * FROM solicitudes WHERE detalles_pago->>'monto' = '1200.50';
SELECT * FROM solicitudes WHERE detalles_pago->>'nit' = '987654321';

-- Actualizaciones
-- Actualizar el estado de una solicitud
UPDATE solicitudes
SET estado = 'completado'
WHERE id = 1;
-- Actualizar un campo en detalles_pago
-- Para actualizar el campo monto en detalles_pago:
UPDATE solicitudes
SET detalles_pago = jsonb_set(detalles_pago, '{monto}', '2500.00'::jsonb)
WHERE id = 2;
-- Agregar un nuevo campo a detalles_pago
-- Para agregar un nuevo campo referencia_pago en detalles_pago:
UPDATE solicitudes
SET detalles_pago = jsonb_set(detalles_pago, '{referencia_pago}', '"REF-2024-001"'::jsonb)
WHERE id = 3;
-- Consultar el nuevo campo añadido
SELECT detalles_pago->>'referencia_pago' AS referencia_pago FROM solicitudes WHERE id = 3;
-- Consulta por el nit:
SELECT * FROM solicitudes WHERE detalles_pago->>'nit' = '555666777';
